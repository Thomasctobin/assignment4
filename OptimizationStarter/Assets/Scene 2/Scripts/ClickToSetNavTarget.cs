﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    public Camera camera;
	// Update is called once per frame
	void Update ()
    {
		//Finds the camera from all the game objects and sets it to camera. 
		//New raycast is a raycast hit.
		//If it hits a raycast, that hits the ground, sets a new nav mesh destination. 
       
        RaycastHit hit = new RaycastHit();
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
