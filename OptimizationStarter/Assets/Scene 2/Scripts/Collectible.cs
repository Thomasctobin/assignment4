﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
		//Creates an array of colliders named collidingColliders and uses physics overlapsphere with a radius set from an initial float.
// Runs a for loop for every single collider. Checks if theyre overlapping, if they do, destroys the collectable object from the scene. 		
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                Destroy( this.gameObject );
            }
        }
    }

}
