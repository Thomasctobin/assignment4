﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
		//Gets the renderer from any of the children, sets the color randomly, and randomizes the rgb. 
        GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
    }

}
