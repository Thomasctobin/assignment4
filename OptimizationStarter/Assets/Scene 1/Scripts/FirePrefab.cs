﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetButton( "Fire1" ) )//Gets input for every frame and fires the balls
		//Sets the click point to the position of the mouse and it is converted into the game world space.
		//Determines the vector for the direction of firing by taking the click point and minuses the game objects position.
		//It then normalizes the fire direction vector and bring it between 0 and 1.
		//It then instatiates the prefab at camera.
		//Takes the prefabs rigidbody and sets its direction and speed of movement. 
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }

}
